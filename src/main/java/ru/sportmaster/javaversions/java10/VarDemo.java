package ru.sportmaster.javaversions.java10;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VarDemo {

    public static void main(String[] args) {

        var i1 = 1;
        var i2 = (Integer) 1;
        var str = "ab";
        var str2 = "";

        Map<Integer, List<String>> order1 = getTravelOrderData();
        Map<Integer, List<String>> order2 = getTravelOrderData();
        Map<Integer, List<String>> order3 = getTravelOrderData();

        var order4 = getTravelOrderData();
        var order5 = getTravelOrderData();
        var order6 = getTravelOrderData();

    }


    public static Map<Integer, List<String>> getTravelOrderData() {
        return Map.of(
                100232, List.of("Иванов И.И.", "Петров П.П."),
                100230, List.of("Потеряева Е.В."),
                101009, List.of("Клюшкин А.Б.", "Пирогов А.Н.")
        );
    }

}
