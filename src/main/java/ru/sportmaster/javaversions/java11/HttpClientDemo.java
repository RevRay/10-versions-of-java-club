package ru.sportmaster.javaversions.java11;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class HttpClientDemo {
    public static void main(String[] args) throws IOException, InterruptedException {
        //подготовили Http-клиент
        HttpClient client = HttpClient.newBuilder().build();
        //подготовили наш запрос
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create("https://ya.ru"))
                .header("happy-header", "happy-value")
                .header("cool-data", "123456")
                .build();
        //выполнили запрос и получили ответ сервера
        HttpResponse<String> response =
                client.send(request, HttpResponse.BodyHandlers.ofString());

        System.out.println(response);
        System.out.println(response.headers());
        System.out.println(response.body());
    }
}
