package ru.sportmaster.javaversions.java11;

public class NewStringMethods {
    public static void main(String[] args) {
        String greeting = "welcome back, ${userName}!";
        String companyName = "\u205FSMLAB ";
        String json =
                "{\n" +
                "  \"uuid\" : \"1ac6f0df-78b7-4cb1-9a5b-7d435a434604\",\n" +
                "  \"name\" : \"ru.smlab.hephaestus.exampletests.ostin.OstinE2ETest\",\n" +
                "  \"children\" : [\n" +
                "    \"86ad035b-ed78-4b33-a784-720f43a84f3c\",\n" +
                "    \"c0fa3ad6-603c-4538-ad04-21e1361ac10c\"\n" +
                "  ],\n" +
                "  \"befores\" : [],\n" +
                "  \"afters\" : [],\n" +
                "  \"start\" : 1662533126452,\n" +
                "  \"stop\" : 1662533126731\n" +
                "}";


        System.out.println(greeting.isBlank()); //vs isEmpty() " "
        System.out.println(companyName.repeat(5)); //"abc" * 5 abcabcabc
        System.out.println(companyName.strip()); //vs trim() - ASCII
        //+stripLeading(), stripTrailing()


        json.lines().filter(s -> s.contains("uuid")).forEach(System.out::println);

    }
}
