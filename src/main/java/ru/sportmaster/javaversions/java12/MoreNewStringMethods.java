package ru.sportmaster.javaversions.java12;

import org.apache.commons.lang3.StringUtils;

public class MoreNewStringMethods {
    public static void main(String[] args) {
        String name = "Alex";
        String veryLongAdviceDescription =
                "этот забытый рецепт приносит богатство - нужно всего лишь работать как проклятый от заката до рассвета.";

        System.out.println(name.indent(30));

        //indent, transform
        System.out.println(StringUtils.abbreviate(veryLongAdviceDescription, 60));
        System.out.println(StringUtils.capitalize(veryLongAdviceDescription));

        String result = veryLongAdviceDescription
                .transform(s -> StringUtils.abbreviate(s, 60))
                .transform(StringUtils::capitalize);

        System.out.println(result);
    }
}
