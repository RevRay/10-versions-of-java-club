package ru.sportmaster.javaversions.java14;

public class SwitchExpressions {
    enum Brand {
        DEMIX,
        COLUMBIA,
        FILA
    }

    public static void main(String[] args) {
        int sales = calculateSalesOldVersion(Brand.DEMIX);
        int sales2 = calculateSalesNewVersion(Brand.DEMIX);
        System.out.println(sales);
    }

    public static int calculateSalesOldVersion(Brand brand) {
        int salesAmount;

        switch (brand) {
            case FILA: {
                salesAmount = 120;
                break;
            }
            case DEMIX: {
                salesAmount = 300;
                break;
            }
            case COLUMBIA: {
                salesAmount = 110;
                break;
            }
            default:
                throw new UnsupportedOperationException();
        }

        return salesAmount;
    }

    //-> , =, return
    //TODO yield
    public static int calculateSalesNewVersion(Brand brand) {

        return switch (brand) {
            case FILA -> 120; //break
            case DEMIX -> {
                System.out.println("");
                yield 300;
            }
            case COLUMBIA -> 110;
            default -> throw new UnsupportedOperationException();
        };
    }
}
