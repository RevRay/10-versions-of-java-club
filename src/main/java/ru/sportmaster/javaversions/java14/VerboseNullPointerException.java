package ru.sportmaster.javaversions.java14;

public class VerboseNullPointerException {

    /*
    Прежний вид сообщения об исключении

    Exception in thread "main" java.lang.NullPointerException
        at ru.sportmaster.javaversions.java14.VerboseNullPointerException.doIt(NPETest.java:19)
        at ru.sportmaster.javaversions.java14.VerboseNullPointerException.main(NPETest.java:14)
     */

    public static void main(String[] args) {
        doIt();
    }

    public static void doIt() {
        String s = null;
        System.out.println(s.length()); //NPE
    }
}
