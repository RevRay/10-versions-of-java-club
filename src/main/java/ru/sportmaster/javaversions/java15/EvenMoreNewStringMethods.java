package ru.sportmaster.javaversions.java15;

public class EvenMoreNewStringMethods {
    public static void main(String[] args) {
        //an example code to be copied
        String s = "abc bcd  search these   nasty excessive  spaces  ";
        s.length();

        //#1 stringObject.stripIndent()
        String messageWithIndentationWhitespaces =
                "  Walter: See, I didn't do it!\n" +
                "  Gus: Do it.";
        System.out.println(messageWithIndentationWhitespaces);
        System.out.println(messageWithIndentationWhitespaces.stripIndent());

        //#2 stringObject.translateEscapes()

        String testEscapes = "В обычном варианте здесь \\n не будет переноса строки";
        System.out.println(testEscapes);
        System.out.println(testEscapes.translateEscapes());

        //#3 stringObject.formatted()

        String oldWay = String.format("looks %s a non-static %s method?", "like", "String.format()");
        String newWay = "looks %s a non-static %s method?".formatted("like", "String.format()");
        System.out.println(newWay);
    }
}
