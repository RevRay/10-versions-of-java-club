package ru.sportmaster.javaversions.java15;

public class TextBlocks {
    public static void main(String[] args) {

        String s1 = "A line";

        //отступы считаются относительно первого символа
        String s2 = """
                Another line""";

        String s3 = """
                Yet another line
                """;


        System.out.println(s1);
        System.out.println(s2);
        System.out.println(s3);
        System.out.println("---");


        printStrings();


    }

    public static void printStrings() {
        String json1 = "{\n" +
                "  \"name\": \"John\",\n" +
                "  \"age\": 30,\n" +
                "  \"car\": null\n" +
                "}";

        String json2 = """
                {
                  "name": "John",
                  "age": 30,
                  "car": null
                }""";

        String lines = """
                - Open the pod bay doors, Hal.
                - I'm sorry, Dave. I'm afraid I can't do that.""";

        System.out.println(json1);
        System.out.println(json2);
        System.out.println(lines);
    }
}
