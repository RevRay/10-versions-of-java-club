package ru.sportmaster.javaversions.java16;

public record CakeAsARecord(String name, int weight) {
}
