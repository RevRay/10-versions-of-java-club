package ru.sportmaster.javaversions.java16;

public class InstanceOf {
    public static void main(String[] args) {
        Object o = "Что я такое?";

        //до
        if (o instanceof String) {
            String s = (String) o;
            System.out.println(s.length());
        } else if (o instanceof Number) {
            Number n = (Number) o;
            System.out.println(n.doubleValue());
        }

        //после
        if (o instanceof String s) {
            System.out.println(s.length());
        } else if (o instanceof Number n) {
            System.out.println(n.doubleValue());
        }

    }
}
