package ru.sportmaster.javaversions.java16;

public class RecordsDemo {
    public static void main(String[] args) {
        CakeAsAClass oldCake = new CakeAsAClass("Наполеон", 120);
        CakeAsARecord newCake1 = new CakeAsARecord("Ленинградский", 145);
        CakeAsARecord newCake2 = new CakeAsARecord("Графские развалины", 90);

        System.out.println(newCake1);
        System.out.println(newCake2.equals(newCake2));
        System.out.println(newCake2.equals(newCake1));
    }
}
