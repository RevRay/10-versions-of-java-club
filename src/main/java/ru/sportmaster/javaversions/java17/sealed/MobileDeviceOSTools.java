package ru.sportmaster.javaversions.java17.sealed;

public sealed class MobileDeviceOSTools permits IOSTools, AndroidOSTools {
}
