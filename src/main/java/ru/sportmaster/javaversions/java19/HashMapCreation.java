package ru.sportmaster.javaversions.java19;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HashMapCreation {
    public static void main(String[] args) {
        //функциональность, доступная до 19й версии
        List<String> myList = new ArrayList<>(25);
        Map<String, Object> myMap = new HashMap<>(100);

        //новый метод
        Map<String, Object> myMap2 = HashMap.newHashMap(200);
    }
}
