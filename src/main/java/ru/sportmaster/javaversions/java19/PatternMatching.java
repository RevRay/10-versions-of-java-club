package ru.sportmaster.javaversions.java19;

import java.util.List;

public class PatternMatching {

    public static void main(String[] args) {
        List<Object> listOfRandomStuff = List.of(new Object(), 135, "status", new UnsupportedOperationException());
        Object obj = "ABcc";
        checkObjectClassWithoutPatternMatching(obj);
        checkObjectClassWithPatternMatching(obj);
    }

    public static void checkObjectClassWithoutPatternMatching(Object obj) {
        Class classType = obj.getClass();
        if (classType == String.class) {
            String s = (String) obj;
            if (s.length() > 5) {
                System.out.println(s.toUpperCase());
            } else {
                System.out.println(s.toLowerCase());
            }
        } else if (classType == Integer.class) {
            Integer i = (Integer) obj;
            System.out.println(i * i);
        } else if (classType == Exception.class) {
            Exception e = (Exception) obj;
            System.out.println("got error: " + e.getMessage());
        } else {
            System.out.println("mismatch");
        }
    }

    public static void checkObjectClassWithPatternMatching(Object obj) {
        switch (obj) {
            case String s when s.length() > 5 -> System.out.println(s.toUpperCase());
            case String s when s.length() < 5 -> System.out.println(s.toUpperCase());
            case String s -> System.out.println(s.toLowerCase());
            case Integer i -> System.out.println(i * i);
            case Exception e -> System.out.println("got error: " + e.getMessage());
            default -> System.out.println("mismatch");
        }
    }


}
