package ru.sportmaster.javaversions.java9;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class CollectionsFactoryMethods {
    public static void main(String[] args) {
        List<Double> physicNumbers = List.of(2.71828, 3.1415, 299792458.0);
        Set<Integer> mysteriousNumbers= Set.of(4, 4, 8, 15, 16, 23, 42);
        Map<String, String> fruitData =
                Map.of("type", "apple", "color", "green", "someKey", "someValue");
    }
}
