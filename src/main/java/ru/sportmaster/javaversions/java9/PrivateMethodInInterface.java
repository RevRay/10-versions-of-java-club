package ru.sportmaster.javaversions.java9;

public interface PrivateMethodInInterface {

    default void calculateAndPrint() {
        int result = calculate();
        printNumber(result);
    }

    private int calculate() {
        return 2 + 2;
    }

    private void printNumber(int number) {
        System.out.println(number);
    }

}
