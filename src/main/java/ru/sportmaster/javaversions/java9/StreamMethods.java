package ru.sportmaster.javaversions.java9;

import java.util.List;
import java.util.stream.Collectors;

public class StreamMethods {
    public static void main(String[] args) {
        List<String> tableElements = List.of("Кроссовки", "Футболка", "", "", "Кепка", "");

        //filter()
        System.out.println(
                tableElements.stream().filter(s -> !s.isBlank()).collect(Collectors.toList())
        );

        //takeWhile()
        System.out.println(
                tableElements.stream().takeWhile(s -> !s.isBlank()).collect(Collectors.toList())
        );

        //dropWhile()
        System.out.println(
                tableElements.stream().dropWhile(s -> s.length() > 5).collect(Collectors.toList())
        );


    }
}
